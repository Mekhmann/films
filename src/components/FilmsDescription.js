import React, { Component } from 'react'
import '../css/FilmsDescription.css'
import Comments from './Comments'

class FilmsDescription extends Component {
  render (){
    const image = this.props.location.state.image;
    const description = this.props.location.state.description;
    const name = this.props.location.state.name;
    return (
      <div className="FilmsDescription">
        <h1>{name}</h1>
        <img src={image} alt="FilmImg"></img>
        <h1>Описание</h1>
        <p className="description">{description}</p>
        <h1 className="comments-header">Комментарии к фильму</h1>
        <div className="comments-block">
          <Comments/>
        </div>
      </div>
    )
  }
}

export default FilmsDescription;