import React from 'react'

export const CommentList = ({ 
    comments, onRemove
    }) => {
    return (
        <ul className="comment-ul">
            {comments.map(comment=>{
                const classes = ['comment']
                return (
                    <li className={classes.join('')} key={comment.id}>
                        <label>
                            <span>{comment.title}</span>
                            <i className="material-icons red-text" onClick={()=>onRemove(comment.id)}>delete</i>
                        </label>
                    </li>
                )
            })}
        </ul>
    )
}

export default CommentList;