import React, { useRef } from "react";

function CommentForm (props) {
    const ref = useRef(null)
    const keyPressHandler = (event) => {
        if (event.key === 'Enter') {
            props.onAdd(ref.current.value)
            ref.current.value = ''
        }
    }

    return (
        <div className="input-field mt2">
            <input
                ref={ref} 
                type="text" 
                id="title" 
                placeholder="Введите комментарий"
                onKeyPress={keyPressHandler} 
            />
        </div>
    )
}

export default CommentForm;