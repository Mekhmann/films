import React, { Component } from 'react'
import '../css/Films.css'
import Pagination from "react-js-pagination"
import {
  Link,
} from "react-router-dom";

class Films extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      activePage: 1,
      startItem: 0,
      endItem: 10,
      size: 10
    };
  }

  handlePageChange(pageNumber) {
    this.setState({activePage: pageNumber});
    this.setState({startItem: (pageNumber-1)*this.state.size});
    this.setState({endItem: (pageNumber-1)*this.state.size + this.state.size});
  }

  componentDidMount() {
    fetch("https://yts.mx/api/v2/list_movies.json")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result.data.movies,
            itemsCount: result.data.movies.length,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Ошибка: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Загрузка...</div>;
    } else {
      return (
        <div>
          <table className="table_blur">
            <thead>
              <tr>
                <th>Название фильма</th>
                <th>Рейтинг</th>
                <th>Год выпуска</th>
                <th>Озвучка</th>
              </tr>
            </thead>
            <tbody>
                {items.slice(this.state.startItem, this.state.endItem).map(item => (
                  <tr key={item.id}>
                    <td>
                      <Link to={{ 
                        pathname: `/${item.id}`, 
                        state: { 
                          'description': item.description_full,
                          'image': item.large_cover_image,
                          'name': item.title     
                        },
                        }}
                      >
                        {item.title}
                      </Link>
                    </td>
                    <td>
                      {item.rating}
                    </td>
                    <td>
                      {item.year}
                    </td>
                    <td>
                      {item.language}
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
          <Pagination
            activePage={this.state.activePage}
            // число фильмов на странице
            itemsCountPerPage={10} 
            // всего фильмов
            totalItemsCount={this.state.itemsCount}
            // видимое количество кнопок пагинации
            pageRangeDisplayed={4}
            onChange={this.handlePageChange.bind(this)}
            prevPageText={'<'}
            nextPageText={'>'}
            hideNavigation={false}
            hideFirstLastPages={true}
          />
        </div>
      );
    }
  }
}

export default Films