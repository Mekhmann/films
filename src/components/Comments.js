import React, { useState } from 'react'
import CommentForm from './CommentForm'
import CommentList from './CommentList'

const Comments  = () => {
      const [comments, setComments] = useState([])

      const addHandler = (title) => {
        const newComment = {
          title: title,
          id: Date.now(),
        }
        setComments(prev=>[newComment, ...prev])
      }

      const removeHandler = (id) => {
        setComments(prev=> prev.filter(comment=>comment.id !== id))
      }
      return (
        <div className="container">
          <CommentForm onAdd={addHandler}/>
          <CommentList comments={comments} onRemove={removeHandler}/>
        </div>
        )
      }
export default Comments;