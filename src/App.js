import React from 'react';
import './css/App.css';
import Films from './components/Films';
import FilmsDescription from './components/FilmsDescription'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
            <Route path="/:id" component={FilmsDescription}>
            </Route>
            <Route exact path="/" component={Films}>
            </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;